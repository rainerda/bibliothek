Simplelibrary

This is a simple spring mvc project.
The purpose of this project is getting to know the spring framework.

Aufgabenstellung:
Erstellen sie eine Webapplikation für eine Bibliothek in der folgende Usecases/Stories abgebildet sind.
1) Eine Übersichtsseite über bestehende Bücher die den Buchnamen, ISBN und Preis anzeigen
a. Für jede Zeile - einen Button (oder Link) um dieses Buch zu löschen
b. Für jede Zeile - einen Link zur Editmaske für dieses Buch
2) Eine Eingabemaske für neue Bücher
3) Eine Editmaske für bestehende Bücher

Bonus für Interessierte:
Zu 1)  Erstellen sie einen Button auf der Übersichtsseite (oberhalb der Liste der Bücher) 
der bei „onClick“ ein Popup öffnet, mit allen Büchernamen (asynchron vom Server geladen - AJAX) 
die zu diesem Zeitpunkt in der Datenbank vorhanden sind.

Die Aufgabenstellungen wurden folgendermaßen gelöst:

1 + 2 Übersichtsseite mit Eingabemaske zum Hinzufügen neuer Bücher
Grund: ich hab beides auf einer Seite zusammengefasst, damit man beim Testen schneller ist.
Für einen echten Kunden würde ich die Funktionalität trennen, für Testzwecke ist es IMHO besser so.
3. Editmaske erreichbar über link bei jedem Buch
Bonusaufgabe:
Ich hab nicht nur die Büchernamen sondern die ganze Liste mit Ajax nachgeladen.
Grund: ich wollte ausprobieren, ob ich das schaffe, die ganze Liste gleichermaßen anzuzeigen.


Verwendete Technologien:
Spring Framework 3.1
Derby mit Hibernate JPA
Tomcat 6.0

Startseite:
http://localhost:8080/Simplelibrary/




