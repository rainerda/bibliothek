<%@page contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>  
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:useBean id="book" type="at.rainerda.simplelibrary.db.Book" scope="request" />


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Simple library - edit page</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
  
	<form:form method="post" action="edit.html" modelAttribute="book">
		<form:hidden path="id"/>
		Name: <form:errors path="name" cssClass="error"></form:errors><br>
		<form:input path="name"/><br><br>
		ISBN:* <form:errors path="isbn" cssClass="error"></form:errors><br>
		<form:input path="isbn"/><br><br>
		Price: <form:errors path="price" cssClass="error"></form:errors><br>
		<form:input path="price" /><br><br>
		
		<input type="submit" value="submit" />
	</form:form>

<p>*(Some valid ISBNs: 3827326222, 3442151473, 978-1-56619-909-4, 978-3-8218-0818-5)</p>

</body>
</html>