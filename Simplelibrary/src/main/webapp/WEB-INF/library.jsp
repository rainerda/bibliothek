<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="book" type="at.rainerda.simplelibrary.db.Book"
	scope="request" />
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
    <head>
        <title>Simple library</title>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>

<!-- Scripting sector -->
		
    	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

		<script type="text/javascript">
		
			//ajax call. get a list of all available books
	        function showBooks() {
	            $.ajax({
	                type : 'GET',
	                url : 'listBooks.html',
	                success : function(data) {
	                   listBooks(data);
	                },
	                error : function() {
	                    alert('error getting books list');
	                }
	            });
	        }	        
	        
	        //open a modal window with a list of all books
	        function listBooks(data) {
				$('#booksModalWindow').dialog({
	                	modal:true,
	                	minWidth:800,
	                	minHeight:600
	                });
				var bookListHtml = '';
				
				bookListHtml = bookListHtml+ '<table class="tg-table-light" style="width: 770px"><tr><th colspan="4" class="tg-center">List of Books</th></tr>';
				bookListHtml = bookListHtml+ '<tr class="tg-odd"><td class="tg-right">No.</td><td>Name</td><td>ISBN</td><td class="tg-right">Price</td></tr>';


	            for ( var i = 0, len = data.length; i < len; ++i) {
	                    var book = data[i];
	                    if (i%2 == 1) {
	                    	bookListHtml = bookListHtml+ '<tr class="tg-odd">';
						} else {
							bookListHtml = bookListHtml+ '<tr>';
						}	                    
	                    bookListHtml = bookListHtml+'<td class="tg-right">'+(i+1)+'</td><td>'+ book.name+ '</td><td>'+book.isbn+'</td><td class="tg-right"> &euro; '+book.price.toFixed(2)+'</td></tr>';
	            }
	            bookListHtml = bookListHtml+ '</table>';
				$('#booksList').empty();
	            $('#booksList').append(bookListHtml);
	        }

   		 </script>
   		 
   		 
 <!-- The title label "Simple Library" -->  		
   		<table class="tg-table-light" style="width: 800px">
          <tr>
		    <th class="tg-center">Simple Library</th>
		  </tr>
		</table>
    
    	<br>
    	

<!-- The Book input form -->
    	
    	<table class="tg-table-light" style="width: 800px">
          <tr>
		    <th class="tg-center">Add new Book</th>
		  </tr>
		</table>
		
		
    	<form:form method="post" action="library.html" modelAttribute="book" cssClass="form">
	    <table class="tg-table-plain" style="width: 800px">
		  <tr>
		    <td>Name: </td>
		    <td><form:input path="name"/></td>
		    <td>ISBN:* </td>
			<td><form:input path="isbn"/></td>
		  </tr>
		  <tr>
		  	<td></td>
		    <td><form:errors path="name" cssClass="error"></form:errors></td>
		    <td></td>
		    <td><form:errors path="isbn" cssClass="error"></form:errors></td>
		    
		  </tr>
		  <tr>
		    <td>Price: &euro; </td>
		    <td><form:input path="price"/></td>
		    <td></td>
		    <td><input type="submit" value="add" /></td>
		  </tr>
		  <tr>
		  	<td></td>
		    <td><form:errors path="price" cssClass="error"></form:errors></td>
		    <td></td>
		    <td></td>
		  </tr>
		</table>
	</form:form>
 
<!--  The button with the ajax list of books-->
<br>
<button id="booksButton" onclick="showBooks()" title="show Books">show Books</button>

<div id="booksModalWindow" title="Ajax List of Books">
	<div id="booksList">
		
	</div>
</div>
		
<!-- The Book List -->
		<br>
        <table class="tg-table-light" style="width: 800px">
          <tr>
		    <th colspan="6" class="tg-center">List of Books</th>
		  </tr>
		  <tr class="tg-odd">
		    <td class="tg-right">No.</td>
		    <td>ISBN</td>
		    <td>Name</td>
		    <td class="tg-right">Price</td>
		    <td></td>
		    <td></td>
		  </tr>
		  <c:forEach var="b" items="${books}" varStatus="rowCounter">
		  	<c:choose>
		    	<c:when test="${rowCounter.count % 2 == 0}">
            		<c:set var="rowStyle" scope="page" value="tg-odd"/>
        		</c:when>
          		<c:otherwise>
            		<c:set var="rowStyle" scope="page" value="tg-even"/>
          		</c:otherwise>
        	</c:choose>
		  <tr class="${rowStyle}">
		  	<td class="tg-right">${rowCounter.count}</td>
		  	<td>${b.isbn}</td>
		  	<td>${b.name}</td>
		  	<td class="tg-right"><fmt:formatNumber value="${b.price}" type="currency" currencySymbol="&euro;"/></td>
		  	<td><a href="edit.html?bookid=${b.id}">edit</a></td>
		  	<td><a href="delete.html?delid=${b.id}" onclick="return confirm('Are you sure?');">remove</a></td>
		  </tr>
		  </c:forEach>
		</table>

		<br>
        <p>*(Note: ISBN verification active. For testing purposes you can use the ISBNs below or google up others.<br>
        Some valid ISBNs: 3827326222, 3442151473, 978-1-56619-909-4, 978-3-8218-0818-5)
        </p>
  	
     </body>
 </html>