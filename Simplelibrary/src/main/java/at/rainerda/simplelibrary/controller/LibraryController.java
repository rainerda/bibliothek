package at.rainerda.simplelibrary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import at.rainerda.simplelibrary.db.Book;
import at.rainerda.simplelibrary.db.BookRepository;
import at.rainerda.simplelibrary.validator.BookValidator;

/**
 * The main controller of the web application.
 * Because this is a rather small project it contains
 * all the request mappings for all pages.
 * @author drainer
 *
 */
@Controller
public class LibraryController {
	

	/**
	 * The book CRUD repository
	 */
	@Autowired
	private BookRepository bookRepository;
	
	/**
	 * The validator for the book forms
	 */
	@Autowired 
	private BookValidator bookValidator;
	
	
	/**
	 * This is the request mapping for the main page.
	 * @param request
	 * @param m
	 * @return
	 */
	@RequestMapping(value="/library", method=RequestMethod.GET)
	public String viewlibrary(Model m){
        
        m.addAttribute("books",bookRepository.findAll());
        m.addAttribute(new Book());
        
		return "library.jsp";
	}
	
	/**
	 * This is the request mapping for deleting a book from the database.
 	 * If a delete id is detected it will delete the corresponding book.
	 * @param delId the book to remove from the database
	 * @param m the model
	 * @return a redirect to the main page
	 */
	@RequestMapping(value="/delete", method=RequestMethod.GET)
	public String deleteBook(@RequestParam("delid") String delId){
		
        if(delId!=null){
        	
        	long idToRemove = Long.valueOf(delId);
        	
        	bookRepository.delete(idToRemove);
        	
        }
		
		return "redirect:library.html";
	}
	

	
	/**
	 * This request mapping for the main page adds a book if the input data is valid.
	 * @param book the book to validate and add
	 * @param result the validation result
	 * @param m the model
	 * @return the main page
	 */
	@RequestMapping(value="/library",method=RequestMethod.POST)
	public String addBook(@ModelAttribute Book book, BindingResult result, Model m){
		
		bookValidator.validate(book, result);
		
		if(result.hasErrors()){
			
			m.addAttribute(book);
			
		}else{
			bookRepository.save(book);
			m.addAttribute(new Book());
		}
		
        m.addAttribute("books",bookRepository.findAll());
        
		return "library.jsp";
	}
	
	/**
	 * Loads the page for editing a book
	 * @param bookId the book id 
	 * @param book the book to edit
	 * @param m the model
	 * @return the link to the edit page
	 */
	@RequestMapping(value="/edit", method=RequestMethod.GET)
	public String loadEditBookForm(@RequestParam("bookid") String bookId, Model m){
		
		if(bookId !=null){
		
			Book currentBook = bookRepository.findOne(Long.valueOf(bookId)); //bookDao.getById(Long.valueOf(bookId));
			m.addAttribute(currentBook);
		}
		
		return "edit.jsp";
	}
	
	/**
	 * Saves the edited book, returning from the edit pages.
	 * If errors occur it will return to the edit form with error messages.
	 * If the form has no errors the book will be saved followed
	 * by a redirect to the main page.
	 * @param m the model
	 * @param book the book to validate and save
	 * @param result the validation result
	 * @return the edit page on error or the main page on success
	 */
	@RequestMapping(value="/edit", method=RequestMethod.POST)
	public String saveEditBookForm( Model m,@ModelAttribute Book book,BindingResult result){
		
		bookValidator.validate(book, result);
		
		if(result.hasErrors()){
			
			m.addAttribute(book);
			
			return "edit.jsp";
			
		}else{
			bookRepository.save(book);
			
			return "redirect:library.html";
		}
		
	}
	
	/**
	 * This request mapping returns a list of all available books for an ajax call.
	 * @return the list of all books in the database
	 */
	@RequestMapping(value="/listBooks")
	public @ResponseBody List<Book>listBooks(){
		
		List<Book> books = bookRepository.findAll();
		return books;
	}
	

}
