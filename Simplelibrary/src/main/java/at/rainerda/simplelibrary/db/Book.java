package at.rainerda.simplelibrary.db;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

/**
 * This class is the book POJO.
 * It is mapped to the database.
 * For CRUD use the BookRepository
 * @author drainer
 *
 */
@Entity
public class Book implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue
	private Long id;
	private String name;
	private String isbn;
	private String shortisbn;
    @NumberFormat(style = Style.NUMBER, pattern = "###,##0.00")
	private double price;
	
	public Book(){
		
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}

	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
		this.setShortisbn(isbn.replace("-", "").trim());
	}

	/**
	 * @return the shortisbn
	 */
	public String getShortisbn() {
		return shortisbn;
	}

	/**
	 * @param isbn the shortisbn to set
	 */
	public void setShortisbn(String shortisbn) {
		this.shortisbn = shortisbn.trim();
		//ensure consistency
		if(this.isbn == null) this.isbn=shortisbn;
		if(this.isbn!=null){
			String toCheck = isbn.replace("-", "").trim();
			if(!toCheck.equals(shortisbn)){
				this.shortisbn = this.isbn.replace("-", "").trim();
			}
		}
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		price=Math.round(price*100.0)/100.0;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Book [name=" + name + ", isbn=" + isbn + ", price=" + price
				+ "]";
	}
	
	public static String toShortISBN(String isbn){
		return isbn.replace("-", "").trim();
	}
	
	

}
