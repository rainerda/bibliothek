/**
 * 
 */
package at.rainerda.simplelibrary.db;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class implements the Book data access object.
 * It is used to create, read, update, delete, find and list all books.
 * @author drainer
 *
 */
@Deprecated
@Component
public class BookDao {

    // Injected database connection:
    @PersistenceContext private EntityManager em;
    
    /**
     * This method adds a new book to the database.
     * For changing a book please use the update method.
     * @param book the new book for the database
     */
    @Transactional
    public void persist(Book book){
    	em.persist(book);
    }
    
    /**
     * This method removes a book with a given id from the database
     * @param bookId the id of the book to remove
     */
    @Transactional
    public void remove(Long bookId){
    	try{
    		Book book = em.find(Book.class, bookId);
    		if(book!=null){
    			em.remove(book);
    		}
    	}catch(Exception ex){
    		
    	}
    }
    
    /**
     * This method returns a list of all books
     * @return a list of all books
     */
    @SuppressWarnings("unchecked")
	public List<Book> getAllBooks(){
    	return em.createQuery(
    			"SELECT b FROM Book b ORDER BY b.id").getResultList();
    }
    
    /**
     * This method returns a book with a given id 
     * or null if the book is not in the database
     * @param id the book id
     * @return the book or null if not found
     */
    public Book getById(Long id){
    	return em.find(Book.class, id);
    	
    }
    
    /**
     * This method updates an existing book in the database
     * @param book the book to update
     */
    @Transactional
    public void updateBook(Book book){
    	updateBook(book.getId(),book.getIsbn(),""+book.getPrice(),book.getName());
    }
    

    /**
     * This method updates an existing book in the database
     * @param bookId the id of the book in the database
     * @param isbn the book isbn
     * @param price the book price
     * @param name the book name
     */
	@Transactional
    public void updateBook(Long bookId, String isbn, String price, String name){
    	try{
    		Book book = em.find(Book.class, bookId);
    		book.setIsbn(isbn);
    		book.setName(name);
    		try{
    			Double priceVal = Double.parseDouble(price);
    		book.setPrice(priceVal);
    		}catch(Exception ex){
    			
    		}
    		em.persist(book);
    	}catch(Exception ex){
    		
    	}
    	

    }
	
	/**
	 * Check if an isbn exists
	 * @param isbn
	 * @return
	 */
	public boolean existsIsbn(String isbn){
		String isbn1 = isbn.replace("-", "");
		boolean result = false;
		if(em!=null){
			@SuppressWarnings("unchecked")
			List<Object> query = em.createQuery("SELECT b FROM Book b WHERE b.shortisbn = '"+isbn1+"'").getResultList();
			result =(!(query == null || query.size()<1));
		}
		return result;
		
	}
    
    
	
}
