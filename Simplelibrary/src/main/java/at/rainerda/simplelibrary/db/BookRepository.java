package at.rainerda.simplelibrary.db;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface BookRepository extends JpaRepository<Book, Long>{
	
	/**
	 * Find all Books with a given isbn in short form
	 * Usage: findByShortisbn(Book.toShortISBN(isbn))
	 * @param shortisbn
	 * @return
	 */
	List<Book> findByShortisbn(String shortisbn);

}
