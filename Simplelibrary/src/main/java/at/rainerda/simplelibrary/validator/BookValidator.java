/**
 * 
 */
package at.rainerda.simplelibrary.validator;
import java.util.List;

import org.apache.commons.validator.routines.ISBNValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import at.rainerda.simplelibrary.db.Book;
import at.rainerda.simplelibrary.db.BookRepository;


/**
 * This class implements a Validator for the Book class.
 * A book must have a non empty name, isbn and price.
 * The isbn must be valid according to the ISBN definition
 * The isbn is verified by the apache commons isbn validator.
 * There are no duplicate isbns allowed in the database.
 * The price must be non negative
 * @author drainer
 *
 */
@Component
public class BookValidator implements Validator{

	public boolean supports(Class<?> clazz) {
		return Book.class.equals(clazz);
	}
	
	@Autowired
	private BookRepository bookRepository;
	
	public BookValidator(){
		super();
	}

	/**
	 * This method validates a book from a book form
	 * @param obj The book (casted to at.rainerda.simplelibrary.db.Book)
	 * @param errors the form errors
	 */
	public void validate(Object obj, Errors errors) {
		Book book = (Book) obj;
		
		//required fields: name, isbn, price
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "isbn", "isbn.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "price.required");
		
		
		
		//check if isbn is valid and does not yet exist if it's a new book
		if( ! errors.hasFieldErrors("isbn")){
		
			
			
			if(!ISBNValidator.getInstance().isValid(book.getIsbn())){
				errors.rejectValue("isbn", "isbn.invalid");
			}else {
				List<Book> books = bookRepository.findByShortisbn(Book.toShortISBN(book.getIsbn()));
				boolean existsISBN = books!=null && books.size()>0;
				
				//check if the id is null (new book) and reject if the isbn already exists.
				//if the book already exists, check if the isbn is the same, if not, there must
				//be a book in the database with the isbn already.
				if(existsISBN){
					if(book.getId()==null) errors.rejectValue("isbn", "isbn.exists");
					else{
						Book duplicate = bookRepository.findOne(book.getId());
						if(duplicate != null && !duplicate.getShortisbn().equals(book.getShortisbn())) errors.rejectValue("isbn", "isbn.exists");
					}
				}
			} 
			
		}
		
		//check if the price is a non negative value
		if( ! errors.hasFieldErrors("price")){
			if(book.getPrice()<0.0){
				errors.rejectValue("price", "price.non_negative");
			}
		}
		
	}
	
	


}
